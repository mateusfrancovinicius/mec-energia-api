import pytest

from users.models import UniversityUser, CustomUser
from utils.user.user_type_util import UserType


class TestIsValidUserType:

    def test_invalid_user_type(self):
        with pytest.raises(Exception, match=r'User type \(invalid_user_type\) does not exist'):
            UserType.is_valid_user_type("invalid_user_type")

    def test_wrong_user_type_and_wrong_user_model(self):
        with pytest.raises(Exception, match=r'Wrong User type \(.+\) for this Model User \(.+\)'):
            UserType.is_valid_user_type(CustomUser.super_user_type, UniversityUser)

    def test_valid_user_type_and_wrong_user_model(self):
        user_type = CustomUser.university_user_type
        user_model = UniversityUser

        result = UserType.is_valid_user_type(user_type, user_model)
        assert result == user_type

    def test_valid_user_type_and_valid_user_model(self):
        user_type = CustomUser.super_user_type
        user_model = CustomUser

        result = UserType.is_valid_user_type(user_type, user_model)
        assert result == user_type

    def test_wrong_custom_user_model_and_wrong_user_type(self):
        with pytest.raises(Exception, match=r'Wrong User type \(.+\) for this Model User \(.+\)'):
            UserType.is_valid_user_type(CustomUser.university_user_type, CustomUser)

    def test_wrong_custom_user_model_and_valid_user_type(self):
        user_type = CustomUser.super_user_type
        user_model = CustomUser

        result = UserType.is_valid_user_type(user_type, user_model)
        assert result == user_type

    def test_valid_custom_user_model_and_valid_user_type(self):
        user_type = CustomUser.super_user_type
        user_model = CustomUser

        result = UserType.is_valid_user_type(user_type, user_model)
        assert result == user_type
